#include <iostream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <rs232.h>

using namespace std;

int main(int argc, char **argv)
{
	//parse arguments
	if(argc < 3)return 1;
	
	uint8_t option = 0, check = 0;
	
	if(memcmp(argv[1], "-r", 2) == 0)option = 1;
	else if(memcmp(argv[1], "-s", 2) == 0)option = 2;
	
	if(option == 1)
	{
		if(strcmp(argv[2], "--checksum") == 0)check = 1;
		else if(strcmp(argv[1], "--crc") == 0)check = 2;
	}
	
	if(option == 1)
	{
		SerialPort port = SerialPort("/dev/ptmx");
		grantpt(port.getFileDesc());
		unlockpt(port.getFileDesc());
		
		uint8_t *data = port.receiveFile(check);
		cout<<data<<endl;
		delete[] data;
		
		sleep(2);//wait for ensure closed transision
	}
	else if(option == 2)
	{
		SerialPort port = SerialPort("/dev/pts/3");
		sleep(5);
		
		char filename[30];
		strcpy(filename, argv[2]);
		
		FILE *file = fopen(filename, "r");
		char *data = new char[10000];
		int length = fread(data, 1, 10000, file);
		fclose(file);
		
		port.sendFile((uint8_t *)data, length);
		delete[] data;
	}
	
	return 0;
}
