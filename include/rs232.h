#ifndef RS232_H
#define RS232_H

#include <cstdint>

#define SOH 0x01
#define EOT 0x04
#define ACK 0x06
#define NAK 0x15
#define CAN 0x18
#define C 0x43


class SerialPort
{
public:
	
	SerialPort(const char *filename);
	~SerialPort();
	
//functions for basic operations
	int readByte();
	int writeByte(uint8_t data);
	
	uint8_t *receiveFile(int checkType);
	void sendFile(uint8_t *file, int length);
	
	uint8_t calculateChecksum(uint8_t *data, int to);
	uint16_t crcChecksum(uint8_t *data, int length);
	
	int getFileDesc(){ return fileDesc; };
		
private:
	
	int fileDesc;

};


#endif//RS232_H
