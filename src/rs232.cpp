#include<rs232.h>

#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdexcept>
#include <string.h>

const uint16_t crc_div = 0x1021;

SerialPort::SerialPort(const char *filename)
{
	fileDesc = open(filename, O_RDWR | O_NOCTTY);
	
	if(fileDesc < 0)throw std::runtime_error("cannot open serial port");
	
	//configure port
	
//get config structure
	struct termios portConf;
	tcgetattr(fileDesc, &portConf);
	
//set bits

	portConf.c_cflag &= ~CSIZE;
	portConf.c_cflag |= CS8;//set 8bit frame
	
	portConf.c_cflag |= CREAD;//enable read
	
	portConf.c_cflag &= ~PARENB;//no parity check
	
	portConf.c_cflag &= ~CSTOPB;//one stop bit
	
	cfsetispeed(&portConf, B115200);
	cfsetospeed(&portConf, B115200);//set clock speeds
	
	
	portConf.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);//raw input
	
	//no character mapping / raw input
	portConf.c_iflag &= ~(ICRNL | INLCR | IGNCR | IUCLC | IXON | IXOFF | IXANY);
	
	portConf.c_oflag &= ~OPOST;//raw output
	
//save config structure
	tcsetattr(fileDesc, TCSANOW, &portConf);
}
///////////////////////////////////////////////////////////////////////

SerialPort::~SerialPort()
{
	if(fileDesc > 0)
		close(fileDesc);
}
////////////////////////////////////////////////////////////////////////

int SerialPort::readByte()
{
	uint8_t data;
		
	read(fileDesc, &data, 1);
	return data;
}
/////////////////////////////////////////////////////////////////////////

int SerialPort::writeByte(uint8_t data)
{
	return write(fileDesc, &data, 1);
}
///////////////////////////////////////////////////////////////////////

void SerialPort::sendFile(uint8_t *fileToSend, int length)
{
	uint8_t resp;
	uint8_t checkType;
	
	//pad file with zeros
	uint8_t *file = new uint8_t[ length + length%128 ];
	memcpy(file, fileToSend, length);
	memset(file+length, 0, length%128);
	
	//wait for ready state of reciver	
	while(1)
	{
		uint8_t type = readByte();
		
		if(type == NAK)
		{
			checkType = 1;
			break;
		}
		if(type == C)
		{
			checkType = 2;
			break;
		}
	};
	
	for(int i=0; i<length; i+=128)
	{
		do
		{
			//send header
			writeByte(SOH);
			writeByte( (uint8_t)(i) /128 + 1);
			writeByte(255 - ( (uint8_t)(i)/128 + 1));
			
			//send block of data
			for(int j=0; j<128; j++)
			{
				writeByte(file[i+j]);
			}
		
			//send checksum
			if(checkType == 1)
				writeByte(calculateChecksum(file+i, 128));
			else
			{
				writeByte( crcChecksum(file+i, 128) >> 8);
				writeByte( crcChecksum(file+i, 128) );
			}
			
			//get response
			resp = 0;
			while(resp != ACK && resp != NAK)resp = readByte();
			
		}while(resp != ACK);
	}
	
	//end of file, end transmission
	do
	{
		writeByte(EOT);
	}while(readByte() != ACK);
	
	delete[] file;
}

/////////////////////////////////////////////////////////////////

uint8_t *SerialPort::receiveFile(int checkType)
{
	uint8_t *data = new uint8_t[32000];
	uint8_t resp, packet, packetCheck, checksum, checksumRec;
	int curIndex = 0, passFlag;
	
	//start connection
	if(checkType == 1)
		writeByte(NAK);
	else
		writeByte(C);
		
	while(1)
	{
		resp = readByte();
		
		if(resp == EOT)
			break;
			
		packet = readByte();
		packetCheck = readByte();
		
		for(int i=0; i<128; i++)
			data[curIndex + i] = readByte();
		
		//check checsum
		passFlag = 1;
		
		if(checkType == 1)
		{
			checksum = calculateChecksum(data+curIndex, 128);
			checksumRec = readByte();
		
			if(checksum != checksumRec || packet != 255-packetCheck)
				passFlag = 0;
		}
		else
		{
			uint16_t crc = 0;
			
			crc = readByte() << 8;
			crc |= readByte();
			
			if( crcChecksum(data+curIndex, 128) != crc)
				passFlag = 0;
		}
			
		
		if(passFlag == 0)
			writeByte(NAK);
		else
		{
			writeByte(ACK);
			curIndex += 128;
		}
	}
	
	//confirm end
	writeByte(ACK);
	
	
	uint8_t *file = new uint8_t[curIndex];
	memcpy(file, data, curIndex);
	delete[] data;
	
	return file; 
}

/////////////////////////////////////////////////////////////////

uint8_t SerialPort::calculateChecksum(uint8_t *data, int length)
{
	uint8_t sum = 0;
	
	for(int i=0; i<length; i++)
		sum += data[i];
			
	return sum;
}

/////////////////////////////////////////////////////////////////

uint16_t SerialPort::crcChecksum(uint8_t *data, int length)
{
	uint16_t crc = 0;
	
	for(int byte=0; byte < length; byte++)
	{
		crc ^= (uint16_t)data[byte] << 8;
		
		for(int i=0; i < 0; i++)
		{
			if(crc & 0x8000 != 0)
			{
				crc = (uint16_t)(crc<<1) ^ crc_div;
			}
			else
			{
				crc = crc << 1;
			}
		}
	}
	
	return crc;
}
